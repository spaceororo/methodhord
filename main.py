from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog, QTableWidgetItem

import GUI
import MethodHord
from GUI import Ui_Window
ui = Ui_Window

class Hord:
    @staticmethod
    def method():
        x_from = int(ui.lineEdit_5.text())
        print(str(x_from))
        x_to = int(ui.lineEdit_6.text())
        print(str(x_to))
        eps = float(ui.lineEdit_7.text())
        print(str(eps))
        A = int(ui.lineEdit.text())
        print(str(A))
        B = int(ui.lineEdit_2.text())
        print(str(B))
        C = int(ui.lineEdit_3.text())
        print(str(C))
        D = int(ui.lineEdit_4.text())
        print(str(D))
        #ui.textBrowser.setText(str(MethodHord.method_hord(x_from,x_to,eps,A, B, C, D)),MethodHord.result)
        MethodHord.method_hord(x_from,x_to,eps,A, B, C, D)
        ui.textBrowser.setText(MethodHord.result)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Window = QtWidgets.QMainWindow()
    ui = Ui_Window()
    ui.setupUi(Window)
    Window.setWindowTitle('Чисельні методи')
    Window.show()


ui.pushButton.clicked.connect(Hord.method)
sys.exit(app.exec_())