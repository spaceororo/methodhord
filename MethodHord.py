import math
result=""
def func(A, B, C,D, x):
    return A*math.pow(x, 3)+B*math.pow(x,2)+C*x+D

def method_hord(x_from, x_to, eps, A, B, C, D):
    global result
    result+=" Розрахунки запишемо в таблицю \nІтервції \n"
    x_step=0
    tmp=0
    n=1
    print(result)
    result+="\n#\tx\tF(x)\th\n"
    while math.fabs(x_step-x_to)>eps:
        tmp = x_step
        print(str(tmp))
        x_step=x_to-func(A, B, C, D, x_to)*(x_from-x_to)/(func(A, B ,C ,D, x_from)-func(A, B ,C ,D, x_to))
        print(str(x_step))
        x_from=x_to
        x_to=tmp
        result+=(str(round(n,3))+"\t"+str(round(x_step,3))+"\t"+str(round(func(A, B, C, D, x_step),3))+"\t"+str(round(math.fabs(x_step-x_to),3))+"\n")
        n+=1
    result += ("\n\n\nВідповіть: корінь нелінійного рівняння в точці x=" + str(x_step))
    return x_step

